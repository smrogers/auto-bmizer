//
//  AppDelegate.h
//  Auto-BMIzer
//
//  Created by Steven Rogers on 12/12/14.
//  Copyright (c) 2014 Steven Rogers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

