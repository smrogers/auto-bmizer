//
//  ViewController.m
//  Auto-BMIzer
//
//  Created by Steven Rogers on 12/12/14.
//  Copyright (c) 2014 Steven Rogers. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    healthStore = [[HKHealthStore alloc] init];
    [self authorizeHealthKit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) update {
    [self updateHeight];
    [self updateWeight];
}

- (void) updateWeight {
    HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
    NSUInteger limit = 1;
    NSSortDescriptor *sortByDateDesc = [NSSortDescriptor sortDescriptorWithKey:HKSampleSortIdentifierEndDate ascending:NO];
    NSArray* sort = [NSArray arrayWithObject:sortByDateDesc];
    
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:type predicate:nil limit:limit sortDescriptors:sort resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            self.weightField.text = @"Error";
            return;
        }
        
        if (results.count <= 0) {
            NSLog(@"Weight not entered!");
            self.weightField.text = @"N/A";
            return;
        }
        
        //
        HKQuantitySample* sample = (HKQuantitySample *)[results lastObject];
        weight = sample.quantity;
        
        [self performSelectorOnMainThread:@selector(updateWeightField) withObject:nil waitUntilDone:NO];
        
        //
        [self performSelectorOnMainThread:@selector(updateBMI) withObject:nil waitUntilDone:NO];
    }];
    
    [healthStore executeQuery:query];
}

- (void) updateHeight {
    HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeight];
    NSUInteger limit = 1;
    NSSortDescriptor *sortByDateDesc = [NSSortDescriptor sortDescriptorWithKey:HKSampleSortIdentifierEndDate ascending:NO];
    NSArray* sort = [NSArray arrayWithObject:sortByDateDesc];
    
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:type predicate:nil limit:limit sortDescriptors:sort resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            self.heightField.text = @"Error";
            return;
        }
        
        if (results.count <= 0) {
            NSLog(@"Weight not entered!");
            self.heightField.text = @"N/A";
            return;
        }
        
        //
        HKQuantitySample* sample = (HKQuantitySample *)[results lastObject];
        height = sample.quantity;
        
        [self performSelectorOnMainThread:@selector(updateHeightField) withObject:nil waitUntilDone:NO];
        
        //
        [self performSelectorOnMainThread:@selector(updateBMI) withObject:nil waitUntilDone:NO];
    }];
    
    [healthStore executeQuery:query];
    
}

- (void) updateBMI {
    if (height == nil || weight == nil) { return; }
    
    double bmi = [self getBMI];
    self.bmiField.text = [NSString stringWithFormat:@"%0.2f", bmi];
}

- (void) updateWithoutHealthData {
    NSLog(@"Health data is not available!");
    
}

- (void) updateWithoutAuthorization {
    NSLog(@"User denied authorization!");
}

- (void) updateHeightField {
    if (height == nil) { return; }
    
    double heightValue = [self getHeight];
    self.heightField.text = [NSString stringWithFormat:@"%0.1f", heightValue];
}

- (void) updateWeightField {
    if (weight == nil) { return; }
    
    double weightValue = [self getWeight];
    self.weightField.text = [NSString stringWithFormat:@"%0.1f", weightValue];
}

- (double) getHeight {
    if (height == nil) { return 0; }
    NSLengthFormatterUnit unit = (self.useMetric.isOn) ? NSLengthFormatterUnitCentimeter : NSLengthFormatterUnitInch;
    return [height doubleValueForUnit:[HKUnit unitFromLengthFormatterUnit:unit]];
}

- (double) getWeight {
    if (weight == nil) { return 0; }
    NSMassFormatterUnit unit = (self.useMetric.isOn) ? NSMassFormatterUnitKilogram : NSMassFormatterUnitPound;
    return [weight doubleValueForUnit:[HKUnit unitFromMassFormatterUnit:unit]];
}

- (double) getBMI {
    double weightValue = [self getWeight];
    double heightValue = [self getHeight];
    
    if (weightValue <= 0 || heightValue <= 0) {
        return 0;
    }
    
    double bmi = weightValue / pow(heightValue, 2);
    bmi *= (!self.useMetric.isOn) ? 703 : 10000;
    
    return bmi;
}

- (void) authorizeHealthKit {
    if (![HKHealthStore isHealthDataAvailable]) {
        [self updateWithoutHealthData];
        return;
    }
    
    // get authorizations
    HKQuantityType* heightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeight];
    HKQuantityType* weightType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass];
    HKQuantityType* bmiType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMassIndex];
    NSSet* write = [NSSet setWithObjects:heightType, weightType, bmiType, nil];
    NSSet* read = [NSSet setWithObjects:heightType, weightType, bmiType, nil];
    
    [healthStore requestAuthorizationToShareTypes:write readTypes:read completion:^(BOOL success, NSError *error) {
        if (!success) {
            [self updateWithoutAuthorization];
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self update];
        });
    }];
}

- (IBAction) useMetricChanged:(id)sender {
    BOOL isOn = self.useMetric.isOn;
    self.weightLabel.text = (isOn) ? @"kg" : @"lbs";
    self.heightLabel.text = (isOn) ? @"cm" : @"inches";
    [self updateHeightField];
    [self updateWeightField];
    [self updateBMI];
}

- (IBAction) updateButtonTapped:(id)sender {
    double bmi = [self getBMI];
    
    if (bmi <= 0) {
        [self showOKAlertWithTitle:@"Error" message:@"Height or missing is missing, so your BMI couldn't be calculated." completion:nil];
        return;
    }
    
    //
    HKQuantityType *type = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMassIndex];
    HKUnit *unit = [HKUnit countUnit];
    HKQuantity *quantity = [HKQuantity quantityWithUnit:unit doubleValue:bmi];
    NSDate *date = [NSDate date];
    HKQuantitySample *sample = [HKQuantitySample quantitySampleWithType:type quantity:quantity startDate:date endDate:date];
    
    [healthStore saveObject:sample withCompletion:^(BOOL success, NSError *error) {
        if (!success) {
            [self showOKAlertWithTitle:@"Error" message:@"There was an error updating your BMI." completion:nil];
            return;
        }
        
        [self showOKAlertWithTitle:@"Success" message:@"Your BMI was updated." completion:nil];
    }];
}

- (void) showOKAlertWithTitle:(NSString *)title message:(NSString *)message completion:(void(^)())completion {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:okButton];

    [self presentViewController:alert animated:YES completion:completion];
}

@end
