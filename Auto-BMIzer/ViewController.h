//
//  ViewController.h
//  Auto-BMIzer
//
//  Created by Steven Rogers on 12/12/14.
//  Copyright (c) 2014 Steven Rogers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HealthKit/HealthKit.h>

@interface ViewController : UIViewController {
    HKHealthStore *healthStore;
    
    HKQuantity* weight;
    HKQuantity* height;
}

@property IBOutlet UILabel *weightLabel;
@property IBOutlet UITextField *weightField;

@property IBOutlet UILabel *heightLabel;
@property IBOutlet UITextField *heightField;

@property IBOutlet UITextField *bmiField;
@property IBOutlet UIButton *updateButton;
@property IBOutlet UISwitch *useMetric;

@end

