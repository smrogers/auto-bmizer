//
//  main.m
//  Auto-BMIzer
//
//  Created by Steven Rogers on 12/12/14.
//  Copyright (c) 2014 Steven Rogers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
